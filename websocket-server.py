import asyncio
from websockets.server import serve

globalReqNum = 0

async def doAsyCmd():
    process = await asyncio.create_subprocess_shell('sleep 5')
    await process.wait()
    process = await asyncio.create_subprocess_exec('ls', '-a', stdout=asyncio.subprocess.PIPE)
    sv, _ = await process.communicate()
    return sv.decode('utf-8')

async def echo(websocket):
    global globalReqNum
    print('RECEIVED')
    async for message in websocket:
        svar = await doAsyCmd()
        print(f'MESSAGE: {message}')
        await websocket.send(f'Request No. {globalReqNum};<br>Answer: {message}<br>Result:<br>{svar}')
        globalReqNum += 1

async def main():
    async with serve(echo, "localhost", 80):
        await asyncio.Future()

asyncio.run(main())